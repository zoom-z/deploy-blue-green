apt update && apt upgrade


# install dependency
apt install apt-transport-https ca-certificates curl software-properties-common


# add key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -


#add repo
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt update
apt-cache policy docker-ce
apt install docker-ce
systemctl status docker


# gitlab runner in docker image
docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest


#register runner in docker image
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
